<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hpachurch');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#Bx3z.l#N<),[ 8jPKK{>+,wSo,m.Ias!gq5U|y((rb-X-`+ywB >{eb87.jcIc,');
define('SECURE_AUTH_KEY',  'RDssW>*xcro5z`,l&C|P%B~1pMM6Zr3}CejT,}tow_~A%kk~auLjw*L-CSrTJX>Z');
define('LOGGED_IN_KEY',    'N:^AicL1ft&HI=|Cs(]>WS-cY3X&rZ+-[%#5vmJ>PMI- AYzZcXLkFn#*<r_gK%N');
define('NONCE_KEY',        'ECkmsvw}nR9{=:9_-#Iz>e~k^dS+|0}g%1^4P}-D$I0wO~1tUDGZ%}UFx<ol+w][');
define('AUTH_SALT',        'fD~CUKsK=H ><WDEU|ztm3}^z}_ET |OPwFJHrlT;z@$L!U7Ew%i+$JX8b[Qs_.%');
define('SECURE_AUTH_SALT', 'x{_4~S7Zc`QSV,+t(B:=Crl(3H6LE]c3?LMtX_C6!+)C #hY+|~9kJ4>-?|>[ET8');
define('LOGGED_IN_SALT',   'n 1@ kGxx5Be6fEJ}_=h5CrJ- kyp|kh,W~d|zpEEzywb4J%?_HxYB)a-3jtu|+R');
define('NONCE_SALT',       '8$-3MvXGV@@|WHGq,rtTG@;KrOc2lnsq+V;5}AS #niJl9cmEeRU;TsE!P+@5]K-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
