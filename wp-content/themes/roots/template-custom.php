<?php
/*
Template Name: Custom
*/
?>

<div class="verse">
<h1>Verse of the Day:</h1>
 <?php echo bible_verse_of_the_day(0); ?>
</div>

<div class="row home-featured">

<div class="col-md-4">
	<div class="inner">
		<h4>New Here</h4>
		<a href="" class="btn btn-primary">Find out More</a>

	</div>
</div>
<div class="col-md-4">
	<div class="inner">
		<h4>Join our Life Group</h4>
		<a href="" class="btn btn-primary">Find a Life Group</a>
	</div>
</div>
<div class="col-md-4">
	<div class="inner">
		<h4>Monthly Bulletin</h4>
		<p>Find out what's happening this month.</p>
		<a href="" class="btn btn-primary">View Bulletin</a>
	</div>
</div>
</div>


<?php get_template_part('templates/content', 'page'); ?>
