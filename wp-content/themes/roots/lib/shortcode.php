<?php
/**
 * Shortcode
 */

function cs_layout_row ( $atts, $content="" ) {
	return'	<div class="row">' . do_shortcode($content) . '</div>	';
}
add_shortcode( 'row', 'cs_layout_row' );

function block_half ( $atts, $content="" ) {
	return'	<div class="col-md-6">' . $content . '</div>	';
}
add_shortcode( 'b_half', 'block_half' );