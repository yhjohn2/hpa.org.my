<footer class="content-info" role="contentinfo">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
    <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
  </div>
</footer>

<script>
	$(function(){
		$.stellar({
			horizontalScrolling: false,
			verticalOffset: 40
		});
	});
    $(function(){
  
  $('.logo img').data('size','big');
    });

    $(window).scroll(function(){
      if($(document).scrollTop() > 120)
    {
        if($('.logo img').data('size') == 'big')
        {
            $('.logo img').data('size','small');
            $('.logo img').stop().animate({
                height:'80px'
            },200);
          
        }
    }
    else
      {
        if($('.logo img').data('size') == 'small')
          {
            $('.logo img').data('size','big');
            $('.logo img').stop().animate({
                height:'150px'
            },200);
          }  
      }
    });
</script>

<?php wp_footer(); ?>
