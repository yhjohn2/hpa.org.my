<div class="subheader">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/subheader.jpg" alt="">
</div>
<div class="container">
	<div class="row">
		<div class="col-md-9">
			<div class="content-wrapper">
			<?php get_template_part('templates/page', 'header'); ?>
			<?php get_template_part('templates/content', 'page'); ?>
			</div>
		</div>
		<div class="col-md-3">
<?php if (roots_display_sidebar()) : ?>
        <aside class="sidebar" role="complementary">
          <?php include roots_sidebar_path(); ?>
        </aside><!-- /.sidebar -->
      <?php endif; ?>
      </div>
  </div>
</div>
