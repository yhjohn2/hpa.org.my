<?php
/*
Template Name: Home Pages
*/
?>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

 <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/header-20years.jpg" alt="...">     
    </div>
    <div class="item">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/header-1000stories.jpg" alt="...">     
    </div>
    <div class="item">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/header-alpha.jpg" alt="...">     
    </div>
    
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>

<div class="welcome">
 <div class="wrap container" role="document">
	<h1>Welcome to Hosanna Praise Assembly</h1>
	<p>We are a church that puts our hope in Jesus Christ... <br>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque 
	<br>bibendum ante sed velit egestas, eu commodo massa placerat. <br>
	 Quisque rhoncus convallis nisl, i</p>
	<a href="" class="btn btn-primary">More</a>
</div>
</div>


<div class="home-featured">
	
 <div class="wrap container" role="document">

<div class="col-md-4">
	<div class="inner">
		<i class="fa fa-question-circle"></i>
		<h4>New Here?</h4>
		<p>Find out more about us.</p>
		<a href="" class="btn btn-primary">Find out More</a>

	</div>
</div>
<div class="col-md-4">
	<div class="inner">
		<i class="fa fa-group"></i>
		<h4>Join our Life Group</h4>
		<p>Find a home near you.</p>
		<a href="life-group" class="btn btn-primary">Join a Life Group</a>
	</div>
</div>
<div class="col-md-4">
	<div class="inner">
		<i class="fa fa-newspaper-o"></i>
		<h4>Monthly Bulletin</h4>
		<p>Find out what's happening this month.</p>
		<a href="" class="btn btn-primary">View Bulletin</a>
	</div>
</div>
</div>
</div>


 <div class="wrap container" role="document">
    <div class="content row">
      <main class="main <?php echo roots_main_class(); ?>" role="main">
      </main><!-- /.main -->
      <?php if (roots_display_sidebar()) : ?>
        <aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
          <?php include roots_sidebar_path(); ?>
        </aside><!-- /.sidebar -->
      <?php endif; ?>
    </div><!-- /.content -->
  </div><!-- /.wrap -->

<div class="upcoming-events">
<div class="wrap container" role="document">
	<div class="row">
  
    	<h1>Upcoming Events:</h1>
    	<div class="col-sm-3">
    		<div class="date">
    			<div class="day">
    				16-18
    			</div>
    			<div class="month">
    				Sept
    			</div>
    			<!-- <div class="year">
    				2014
    			</div> -->
    		</div>
    		<div class="event-title">
    			<h4>Youth Retreat</h4>
    			<a href="">Read More</a>
    		</div>
    	</div>
    	<div class="col-sm-3">
    		<div class="date">
    			<div class="day">
    				11
    			</div>
    			<div class="month">
    				Oct
    			</div>
    			<!-- <div class="year">
    				2014
    			</div> -->
    		</div>
    		<div class="event-title">
    			<h4>Annual Alpha Ministry Training</h4>
    			<a href="">Read More</a>
    		</div>
    	</div>
    	<div class="col-sm-3">
    		<div class="date">
    			<div class="day">
    				22
    			</div>
    			<div class="month">
    				Oct
    			</div>
    			<!-- <div class="year">
    				2014
    			</div> -->
    		</div>
    		<div class="event-title">
    			<h4>20th Anniversary Celebrations</h4>
    			<a href="">Read More</a>
    		</div>
    	</div>
    	<div class="col-sm-3">
    		<div class="date">
    			<div class="day">
    				25
    			</div>
    			<div class="month">
    				Dec
    			</div>
    			<!-- <div class="year">
    				2014
    			</div> -->
    		</div>
    		<div class="event-title">
    			<h4>Christmas Celebrations</h4>
    			<a href="">Read More</a>
    		</div>
    	</div>
    	</div>
    	
    	<div class="row">
    	<a class="btn btn-success">View All Events</a>
    	</div>
    	
  
  </div><!-- /.wrap -->
  </div>
<!-- <div class="verse">
<h1>Verse of the Day:</h1>
 <?php echo bible_verse_of_the_day(0); ?>
</div>

<div class="row home-featured">

<div class="col-md-4">
	<div class="inner">
		<h4>New Here</h4>
		<a href="" class="btn btn-primary">Find out More</a>

	</div>
</div>
<div class="col-md-4">
	<div class="inner">
		<h4>Join our Life Group</h4>
		<a href="" class="btn btn-primary">Find a Life Group</a>
	</div>
</div>
<div class="col-md-4">
	<div class="inner">
		<h4>Monthly Bulletin</h4>
		<p>Find out what's happening this month.</p>
		<a href="" class="btn btn-primary">View Bulletin</a>
	</div>
</div>
</div> -->


<?php get_template_part('templates/content', 'page'); ?>
