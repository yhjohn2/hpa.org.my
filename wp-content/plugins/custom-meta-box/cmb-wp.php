<?php
/*
Plugin Name: Poetfarmer Custom Meta Boxes
Plugin URI: http://www.elluminatedesigns.com/
Description: Create Meta Boxes for Poetfarmer.
Version: 0.1
Author: Elluminate Designs
Author URI: http://www.elluminatedesigns.com/
License: GPL v2 or higher
License URI: License URI: http://www.gnu.org/licenses/gpl-2.0.html

Every Meta Box Is Targetted at specific post type / pages (using page ID)
Look at the comments for info
*/


//Initialize the metabox class
function wpb_initialize_cmb_meta_boxes() {
    if ( ! class_exists( 'cmb_Meta_Box' ) )
        require_once(plugin_dir_path( __FILE__ ) . 'init.php');
}

add_action( 'init', 'wpb_initialize_cmb_meta_boxes', 9999 );
//Add Meta Boxes
function wpb_sample_metaboxes( $meta_boxes ) {
    $prefix = '_wpb_'; // Prefix for all fields

    //Home page client's Logo
    $meta_boxes[] = array(
        'id' => 'test_metabox',
        'title' => 'Test Metabox',
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 38, ), ), // Home page client's logo
        'fields' => array(
            array(
                'name' => 'Client Logo 1',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'client_logo_1',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),

            array(
                'name' => 'Client Logo 2',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'client_logo_2',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),

            array(
                'name' => 'Client Logo 3',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'client_logo_3',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),

            array(
                'name' => 'Client Logo 4',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'client_logo_4',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),

            array(
                'name' => 'Client Logo 5',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'client_logo_5',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),

            array(
                'name' => 'Client Logo 6',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'client_logo_6',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),

            array(
                'name' => 'Client Logo 7',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'client_logo_7',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),

            array(
                'name' => 'Client Logo 8',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'client_logo_8',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),
        ),
    );
// Home page oridomi
    $meta_boxes[] = array(
        'id' => 'home_oridomi',
        'title' => 'Oridoimi',
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 27, ), ), // Home oridomi page
        'fields' => array(
            array(
                'name' => 'Oridomi Image 1',
                'desc' => 'Upload an image',
                'id' => $prefix . 'oridomi_home_image_1',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
            array(
                'name' => 'Oridomi Image 2',
                'desc' => 'Upload an image',
                'id' => $prefix . 'oridomi_home_image_2',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
            array(
                'name' => 'Oridomi Image 3',
                'desc' => 'Upload an image',
                'id' => $prefix . 'oridomi_home_image_3',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
            ),
        );

$meta_boxes[] = array(
        'id' => 'contact_panel',
        'title' => 'Contact Panels',
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 426, ), ), // home contact panel
        'fields' => array(
            array(
                'name' => 'Contact Us',
                'desc' => '&lt;h1&gt; for title, &lt;br&gt; for breaks &lt;a$gt; for buttons',
                'default' => '<h1>Title</h1> Description <a href="mailto:info@poetfarmer.com" class="button">EMAIL US</a>',
                'id' => $prefix . 'contact_panel_1',
                'type' => 'textarea'
                ),
            array(
                'name' => 'Join Us',
                'desc' => '&lt;h1&gt; for title, &lt;br&gt; for breaks &lt;a$gt; for buttons',
                'default' => '<h1>Title</h1> Description <a href="mailto:info@poetfarmer.com" class="button">EMAIL US</a>',
                'id' => $prefix . 'contact_panel_2',
                'type' => 'textarea'
                ),
            ),
        );

// Meta box for case overview 
$meta_boxes[] = array(
        'id' => 'case_overview_desc',
        'title' => 'Case Overview Description',
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 73, ), ), // Case Studies Page
        'fields' => array(
            array(
                'name' => 'Description Text 1',
                'desc' => 'html tags apply',
                'default' => 'Description Text Here',
                'id' => $prefix . 'cs_desc_1',
                'type' => 'textarea_small'
                ),
            array(
                'name' => 'Description Text 2',
                'desc' => 'html tags apply',
                'default' => 'Description Text Here',
                'id' => $prefix . 'cs_desc_2',
                'type' => 'textarea_small'
                ),
            array(
                'name' => 'Description Text 3',
                'desc' => 'html tags apply',
                'default' => 'Description Text Here',
                'id' => $prefix . 'cs_desc_3',
                'type' => 'textarea_small'
                ),
            ),
        );

// Meta box for case studies individual

    $meta_boxes[] = array(
        'id' => 'case_studies_custom',
        'title' => 'Case Studies Custom',
        'pages' => array('case-study'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        // 'show_on'    => array( 'key' => 'id', 'value' => array( 38, ), ), // Home page client's logo
        'fields' => array(
            array( // color picker
                'name' => 'Featured Background Color',
                'id'   => $prefix . 'cs_colorpicker',
                'type' => 'colorpicker',
                'default'  => '#ffffff',
                'repeatable' => false,
            ),
            array( // background image
                'name' => 'Case Study Detail Background',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'cs_detail_bg',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),
            array( // Foreground Image
                'name' => 'Case Study Detail Foreground',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'cs_detail_img',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),
            array( // Foreground Image
                'name' => 'Secondary Featured Image',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'cs_second_featured_image',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),
        ),
    );



// Meta box for Home Carousel

   $meta_boxes[] = array(
        'id' => 'home_carousel',
        'title' => 'Home Carousel',
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 36, ), ), // Home page carousel item
        'fields' => array(
             array(
                'name' => 'Carousel Image 1',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'carousel_image_1',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
             array(
                'name' => 'Carousel Text 1',
                'desc' => 'Use &lt;h1&gt;&lt;/h1&gt; text for heading, &lt;p&gt;&lt;/p&gt; for sub text and &lt;br&gt for breaks',
                'default' => '<h1>Heading Text Here</h1><p>Sub Text Here</p>',
                'id' => $prefix . 'carousel_text_1',
                'type' => 'textarea_small'
                ),
             array(
                'name' => 'Carousel Image 2',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'carousel_image_2',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
             array(
                'name' => 'Carousel Text 2',
                'desc' => 'Use &lt;h1&gt;&lt;/h1&gt; text for heading, &lt;p&gt;&lt;/p&gt; for sub text and &lt;br&gt for breaks',
                'default' => '<h1>Heading Text Here</h1><p>Sub Text Here</p>',
                'id' => $prefix . 'carousel_text_2',
                'type' => 'textarea_small'
                ),
             array(
                'name' => 'Carousel Image 3',
                'desc' => 'Upload an image or enter an URL.',
                'id' => $prefix . 'carousel_image_3',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
             array(
                'name' => 'Carousel Text 3',
                'desc' => 'Use &lt;h1&gt;&lt;/h1&gt; text for heading, &lt;p&gt;&lt;/p&gt; for sub text and &lt;br&gt for breaks',
                'default' => '<h1>Heading Text Here</h1><p>Sub Text Here</p>',
                'id' => $prefix . 'carousel_text_3',
                'type' => 'textarea_small'
                ),
            ),
        );

// About page oridomi
$meta_boxes[] = array(
        'id' => 'about_oridomi',
        'title' => 'Oridoimi',
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 24, ), ), // About Page item
        'fields' => array(
            array(
                'name' => 'Oridomi Image 1',
                'desc' => 'Upload an image',
                'id' => $prefix . 'oridomi_abt_image_1',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
            array(
                'name' => 'Oridomi Image 2',
                'desc' => 'Upload an image',
                'id' => $prefix . 'oridomi_abt_image_2',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
            array(
                'name' => 'Oridomi Image 3',
                'desc' => 'Upload an image',
                'id' => $prefix . 'oridomi_abt_image_3',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
            ),
        );

// About Page -- Profiles
    $meta_boxes[] = array(
        'id' => 'profile_1',
        'title' => 'Profile 1',
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 24, ), ), // About Page item
        'fields' => array(
             array(
                'name' => 'Profile Image',
                'desc' => 'Upload an image',
                'id' => $prefix . 'profile_image_1',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
             array(
                'name' => 'Profile Info',
                'desc' => 'Use &lt;h1&gt;&lt;/h1&gt; text for Name, &lt;p&gt;&lt;/p&gt; for taglines, &lt;br&gt for breaks',
                'default' => '<h1>Name</h1><p>Tagline</p>',
                'id' => $prefix . 'profile_info_1',
                'type' => 'textarea_small'
                ),
             array(
                'name' => 'Email Address',
                'id'   => $prefix . 'profile_email_1',
                'desc' => 'Email Address',
                'type' => 'text_email',
                ),
             array(
                'name' => __( 'Twitter Profile', 'cmb' ),
                'id'   => $prefix . 'profile_twitter_1',
                'desc' => 'https://twitter.com/username',
                'type' => 'text_url',
                ),
             array(
                'name' => __( 'Linkedin Profile', 'cmb' ),
                'id'   => $prefix . 'profile_linkedin_1',
                'desc' => 'https://www.linkedin.com/in/username',
                'type' => 'text_url',
                ),
            ),
        );
$meta_boxes[] = array(
        'id' => 'profile_2',
        'title' => 'Profile 2',
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 24, ), ), // About Page item
        'fields' => array(
             array(
                'name' => 'Profile Image',
                'desc' => 'Upload an image',
                'id' => $prefix . 'profile_image_2',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
             array(
                'name' => 'Profile Info',
                'desc' => 'Use &lt;h1&gt;&lt;/h1&gt; text for Name, &lt;p&gt;&lt;/p&gt; for taglines, &lt;br&gt for breaks',
                'default' => '<h1>Name</h1><p>Tagline</p>',
                'id' => $prefix . 'profile_info_2',
                'type' => 'textarea_small'
                ),
             array(
                'name' => 'Email Address',
                'id'   => $prefix . 'profile_email_2',
                'desc' => 'Email Address',
                'type' => 'text_email',
                ),
             array(
                'name' => __( 'Twitter Profile', 'cmb' ),
                'id'   => $prefix . 'profile_twitter_2',
                'desc' => 'https://twitter.com/username',
                'type' => 'text_url',
                ),
             array(
                'name' => __( 'Linkedin Profile', 'cmb' ),
                'id'   => $prefix . 'profile_linkedin_2',
                'desc' => 'https://www.linkedin.com/in/username',
                'type' => 'text_url',
                ),
            ),
        );
$meta_boxes[] = array(
        'id' => 'profile_3',
        'title' => 'Profile 3',
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 24, ), ), // About Page item
        'fields' => array(
             array(
                'name' => 'Profile Image',
                'desc' => 'Upload an image',
                'id' => $prefix . 'profile_image_3',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                ),
             array(
                'name' => 'Profile Info',
                'desc' => 'Use &lt;h1&gt;&lt;/h1&gt; text for Name, &lt;p&gt;&lt;/p&gt; for taglines, &lt;br&gt for breaks',
                'default' => '<h1>Name</h1><p>Tagline</p>',
                'id' => $prefix . 'profile_info_3',
                'type' => 'textarea_small'
                ),
             array(
                'name' => 'Email Address',
                'id'   => $prefix . 'profile_email_3',
                'desc' => 'Email Address',
                'type' => 'text_email',
                ),
             array(
                'name' => __( 'Twitter Profile', 'cmb' ),
                'id'   => $prefix . 'profile_twitter_3',
                'desc' => 'https://twitter.com/username',
                'type' => 'text_url',
                ),
             array(
                'name' => __( 'Linkedin Profile', 'cmb' ),
                'id'   => $prefix . 'profile_linkedin_3',
                'desc' => 'https://www.linkedin.com/in/username',
                'type' => 'text_url',
                ),
            ),
        );
// About Careers

   $meta_boxes[] = array(
        'id' => 'careers',
        'title' => 'Career 1',
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 257, ), ), // About Page job
        'fields' => array(
            array( // color picker
                'name' => 'Career Background Color',
                'id'   => $prefix . 'career_bg_color_1',
                'type' => 'colorpicker',
                'default'  => '#ffffff',
                'repeatable' => false,
                ),
            array(
                'name' => 'Career Title',
                'desc' => 'Career Title',
                'default' => 'Career Title',
                'id' => $prefix . 'career_title_1',
                'type' => 'textarea_small'
                ),
            array(
                'name' => 'Career Description',
                'desc' => 'Career Description',
                'default' => 'Career Description',
                'id' => $prefix . 'career_description_1',
                'type' => 'textarea'
                ),
            ),
        );
    $meta_boxes[] = array(
        'id' => 'careers_2',
        'title' => 'Career 2',
        'pages' => array('page'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 257, ), ), // About Page job
        'fields' => array(
            array( // color picker
                'name' => 'Career Background Color',
                'id'   => $prefix . 'career_bg_color_2',
                'type' => 'colorpicker',
                'default'  => '#ffffff',
                'repeatable' => false,
                ),
            array(
                'name' => 'Career Title',
                'desc' => 'Career Title',
                'default' => 'Career Title',
                'id' => $prefix . 'career_title_2',
                'type' => 'textarea_small'
                ),
            array(
                'name' => 'Career Description',
                'desc' => 'Career Description',
                'default' => 'Career Description',
                'id' => $prefix . 'career_description_2',
                'type' => 'textarea'
                ),
            ),
        );

    return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'wpb_sample_metaboxes' );